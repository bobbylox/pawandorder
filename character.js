G.Character = function(name){
	this.children = [];
	this.layers = {};
	this.name = name;
	this.dialogue = null;
	var that = this;
	var spec = G.character_spec[name];
	
	//Hook for later removal
	G.conditions.set(name+"_leave", 
		function(){
			createjs.Tween.get(that).to({alpha:0}, 1000);
		}
	);
	
	//Background Image
	var bg = new createjs.Bitmap(G.queue.getResult(spec.bg));
	that.addChild(bg);
	
	//bounds
	var bitBounds = bg.getBounds();
	var geom = new createjs.Shape();
	geom.graphics.beginFill("#F00").dr(0,0,bitBounds.width, bitBounds.height);
	
	//animations
	if(spec.layers){
		var layerspec;
		var layer;
		for(var i in spec.layers){
			layerspec = spec.layers[i];
			if(layerspec.type=="bitmap"){
				layer = new createjs.Bitmap(G.queue.getResult(layerspec.id));
			}
			else if(layerspec.type=="toggle"){
				layer = new G.Toggle(layerspec.data);//maybe specify conditions
			}
			else if(layerspec.type=="examinable"){
				layer = new G.Examinable(layerspec.w,layerspec.h,layerspec.id,{});
			}
			else if(layerspec.type=="sprite"){
				var sheet = new createjs.SpriteSheet(layerspec.data);
				layer = new createjs.Sprite(sheet, "animate");
			}
			//this should be valid for all layer types
			this.layers[layerspec.id] = layer;
			layer.x = layerspec.x||0;
			layer.y = layerspec.y||0;
			this.addChild(layer);
		}
	}
	
	//bounds
	var bitBounds = bg.getBounds();
	var geom = new createjs.Shape();
	geom.graphics.beginFill("#F00").dr(0,0,bitBounds.width, bitBounds.height);
	
	//function
	this.talk = function(){
		if(!that.dialogue){
			that.dialogue = new G.Dialogue.Whole(that.name);
		}
		//console.log(that.dialogue);
		that.dialogue.alpha = 1;
		G.stage.addChildAt(that.dialogue, G.stage.children.length-2);
		that.dialogue.say_next();
	}
	
	this.hotspot = new G.Hotspot(geom, this.talk, "talk");
	this.addChild(this.hotspot);
};
G.Character.prototype = new createjs.Container();

G.Dialogue = {};

G.Dialogue.Whole = function(name){
	this.children = [];
	var that = this;
	this.name = name;
	//the dialogue tree and our current place in it
	var tree = G.dialogue_tree[name];
	var place = [];
	this.add_choice = function(choice_string){
		place.push(choice_string);
	};
	this.back = function(){
		place.pop();
		that.show_choices();
	};
	//the screen dimensions
	var screen_dim = G.stage.getBounds();
	//the background
	var bg = new createjs.Shape();
	bg.graphics.beginFill("#000").drawRect(0,0,screen_dim.width,screen_dim.height);
	bg.alpha = 0.7;
	
	this.addChild(bg);
	
	//the image of the character (will be combined with a sprite somehow later)
	this.emotion = tree.emotion||"neutral";
	this.image = new createjs.Bitmap(G.queue.getResult(name+"_cu_"+this.emotion));
	this.addChild(this.image);
	var img_dim = this.image.getBounds()||{width:25, height:25};
	this.image.x = (screen_dim.width-img_dim.width)*0.5;
	this.image.y = (screen_dim.height-img_dim.height)*0.5;
	var no_spot = new G.Hotspot(bg, G.nothing);
	this.addChild(no_spot);//this is just to make it touch-opaque
	
	//Now to add the branching dialogue system!
	this.fadeout = function(time){
		if(!time){
			time = 1;
		}
		createjs.Tween.get(that).to({alpha:0},time*1000).call(that.remove);
	}
	this.remove = function(){
		//this.removeAllChildren();
		//this.removeAllEventListeners();
		createjs.Tween.get(G.timer).to({y:10},500);
		G.stage.removeChild(that);
	}
	
	var img_shape = new createjs.Shape();
	img_shape.graphics.beginFill("#000").dr(0,0,img_dim.width,img_dim.height);
	var go_away = new G.Hotspot(img_shape, that.fadeout);
	go_away.x = this.image.x;
	go_away.y = this.image.y;
	this.addChild(go_away);//now you can dismiss dialogue
	
	this.subtree = function(){
		var sub = tree;
		for(var i=0; i<place.length; i++){
			sub = sub.choices[place[i]];
		}
		return sub;
	}
	
	//our choices for responding
	this.show_choices = function(){
		//console.log("show_choices called");
		var new_choices = function(){
			if(that.choices){
				that.choices.removeAllChildren();
				that.choices.removeAllEventListeners();
				that.removeChild(that.choices);
				that.choices = null;
			}
			if(that.subtree().choices){
				that.choices = new G.Dialogue.Choices(that.subtree().choices);
				that.addChild(that.choices);
				//console.log("added new choices");
				that.choices.x = 0;
				that.choices.y = screen_dim.height;
				var choice_bounds = that.choices.getBounds();
				createjs.Tween.get(that.choices,{override:true}).to({y:screen_dim.height-choice_bounds.height},500);
			}
			else{
				that.fadeout();
			}
		}
		
		if(that.choices){
			//hide the old choices, if any
			createjs.Tween.get(that.choices, {override:true}).to({y:screen_dim.height}, 500).call(new_choices);
		}
		else{
			new_choices();
		}
	}
	
	//What the Character Says:
	this.utterance = new G.Dialogue.Utterance();
	this.addChild(this.utterance);
	this.say_next = function(){
		//console.log("say_next called");
		that.utterance.say(that.subtree().utterance, that.show_choices);
	}
	this.say_next();
	createjs.Tween.get(G.timer).to({y:100},500);
};
G.Dialogue.Whole.prototype = new createjs.Container();

G.Dialogue.Utterance = function(){
	//Text
	this.children = [];
	var margin = 15;
	var text = new createjs.Text("","36pt Patrick Hand", "#000");
	var screen_dim = G.stage.getBounds();
	text.lineWidth = screen_dim.width-margin*2;
	text.x = margin;
	text.y = margin;
	var bottom = text.getMeasuredHeight()+margin*4;
	
	var chars = {c:0};
	
	this.say = function(new_text, completeFn){
		//TODO: change these to typewriter classes
		chars = {c:0};
		if(!completeFn){
			completeFn = G.nothing;
		}
		var add_chars = function(){
			text.text = new_text.substr(0,chars.c);
		}                                                                                                                                   
		if(new_text){
			createjs.Tween.get(chars).to({c:new_text.length}, new_text.length*50).call(completeFn).addEventListener("change",add_chars);
		}
	}
	
	this.addChild(text);
	
	//Background
	var bg = new createjs.Shape();
	var screen_dim = G.stage.getBounds();
	bg.graphics.ss(4,1,1).s("#000").beginFill("#FFF").mt(0,0).lt(screen_dim.width,0);
	bg.graphics.lt(screen_dim.width,bottom).lt(75,bottom).lt(100, bottom+50).lt(0,bottom).cp();
	this.addChildAt(bg,0);
}
G.Dialogue.Utterance.prototype = new createjs.Container();

G.Dialogue.Choices = function(choices){
	this.bg = new createjs.Shape();
	var screen_dim = G.stage.getBounds();
	var margin = 15;
	var gap = 10;
	var choice_text = [];
	var current_text;
	var that = this;
	
	var hover_choice = function(event){//someone mouses over a choice
		event.target.color = "#00F";
	}
	var unhover_choice = function(event){//mouse leaves the choice
		event.target.color = "#000";
	}
	var handle_choice = function(event){//what if someone chooses this?
		var tree = choices[event.target.text];
		var go_again = false;
		if(tree.action){
			if(tree.action.give){
				G.inventory.remove(tree.action.give);
			}
			if(tree.action.get){
				G.inventory.add(tree.action.get);
			}
			if(tree.action.end){
				that.parent.fadeout();
			}
			if(tree.action.back){
				go_again = true;
				if(tree.action.back>1){
					for(var i=tree.action.back; i>1; i--){
						that.parent.back();
					}
				}
			}
			if(tree.action.condition){
				for(var i in tree.action.condition){
					G.conditions.met(tree.action.condition[i]);
				}
			}
			if(tree.action.open){
				var drs = G.doors.getDoors(G.scene.name, tree.action.open);
				for(var d in drs){
					drs[d].unlock();
				}
			}
		}
		if(!go_again){
			that.parent.add_choice(event.target.text);
			that.parent.say_next();
		}
		else{
			that.parent.utterance.say(tree.utterance);//advance what char says only
		}
	}
	var depth = margin;
	for(var i in choices){
		current_text = new createjs.Text(i, "24pt Patrick Hand", "#000");
		current_text.lineWidth = screen_dim.width-margin*2;
		current_text.textAlign = "left";
		current_text.x = margin;
		current_text.y = depth;
		choice_text.push(current_text);
		
		current_text.addEventListener("rollover", hover_choice);
		current_text.addEventListener("rollout", unhover_choice);
		current_text.addEventListener("pressup", handle_choice);
		
		this.addChild(current_text);
		
		depth += current_text.getMeasuredHeight()+gap;
	}
	this.bg.graphics.ss(4,1,1).s("#000").beginFill("#FFF").drawRect(0,0,screen_dim.width,depth+margin);
	this.setBounds(0,0,screen_dim.width,depth+margin);
	this.addChildAt(this.bg, 0);
};
G.Dialogue.Choices.prototype = new createjs.Container();
