G.character_spec = {
	"sofia":{
		"bg":"sofia",
		"layers":[
			{
				"id":"smoke",
				"type":"sprite",
				"x":-44,
				"y":110,
				"data":{
					"images": ["assets/img/character/sofia_stand_anim.png"],
					"frames": [

						[2, 2, 45, 56], 
						[49, 2, 45, 56], 
						[96, 2, 45, 56], 
						[143, 2, 45, 56], 
						[190, 2, 45, 56], 
						[237, 2, 45, 56], 
						[284, 2, 45, 56], 
						[331, 2, 45, 56], 
						[378, 2, 45, 56], 
						[425, 2, 45, 56], 
						[472, 2, 45, 56], 
						[519, 2, 45, 56]
					],
					"animations": {
						"animate":{
							"frames":[0,1,2,3,4,5,6,7,8,9,10,11],
							"speed":0.5
						}
					}
				}
			}
		]
	},
	"mike":{
		"bg":"mike",
		"layers":[
			{
				"id":"arm",
				"type":"sprite",
				"x":0,
				"y":130,
				"data":{
					"images": ["assets/img/character/mike_stand_anim.png"],
					"frames": [

						[2, 2, 27, 129], 
						[2, 133, 27, 129], 
						[2, 264, 27, 129], 
						[2, 395, 27, 129]
					],
					"animations": {
						"animate":{
							"frames":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,2,3,1],
							"speed":0.5
						}
					}
				}
			}
		]
	},
	"jenny":{
		"bg":"jenny"
	},
	"sofia_sit":{
		"bg":"sofia_sit",
		"layers":[
			{
				"id":"page_turn",
				"type":"sprite",
				"data":{
					"images": ["assets/img/character/sofia_sit_anim.png"],
					"frames": [
						[2, 2, 56, 40], 
						[2, 44, 56, 40], 
						[2, 86, 56, 40], 
						[2, 128, 56, 40], 
						[2, 170, 56, 40], 
						[2, 212, 56, 40], 
						[2, 254, 56, 40], 
						[2, 296, 56, 40], 
						[2, 338, 56, 40], 
						[2, 380, 56, 40]
					],
					"animations": {
						"animate":{
							"frames":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9],
							"speed":0.4
						}
					}
				},
				"x":160,
				"y":65
			}
		]
	},
	"kevin":{
		"bg":"kevin",
		"layers":[
		
		]
	}
}