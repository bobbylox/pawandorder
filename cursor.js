G.cursor = {};

G.cursor.init = function(){
	G.cursor.state = "up";
	G.cursor.setImage("default");
	//EVENTS
	G.stage.addEventListener("stagemousemove", G.cursor.setPosition);
	//G.stage.addEventListener("pressmove", G.cursor.setPosition);
	G.stage.addEventListener("stagemouseup", G.cursor.up);
	//G.stage.addEventListener("pressup", G.cursor.up);
	G.stage.addEventListener("stagemousedown", G.cursor.down);
}

G.cursor.setImage = function(name, item){
	//console.log("setting cursor image to "+name);
	if(G.cursor.img){
		G.stage.removeChild(G.cursor.img);
		var prevX = G.cursor.img.x;
		var prevY = G.cursor.img.y;
	}
	if(name!=G.cursor.name){
		G.cursor.prevName = G.cursor.name;
	}
	G.cursor.name = name||"default";
	G.cursor.item = name?item:false;//you can only set item to true if there is also a name
	
	var suffix = G.cursor.state;
	var fullName = name;
	if(!item){
		fullName = "cursor"+name+suffix;
	}
	G.cursor.img = new createjs.Bitmap(G.queue.getResult(fullName));
	G.cursor.img.x = prevX||0;
	G.cursor.img.y = prevY||0;
	G.stage.addChild(G.cursor.img);
};

G.cursor.setState = function(upordown){
	//console.log("setting cursor state to "+upordown);
	if(upordown=="up" || upordown=="down"){
		G.cursor.state = upordown;
	}
	if(!G.cursor.item){
		G.cursor.setImage(G.cursor.name);
	}
	else{
		if(upordown=="up"){
			G.cursor.lighten();
		}
		else{
			G.cursor.darken();
		}
	}
};
G.cursor.up = function(event){G.cursor.setState("up");G.cursor.setPosition(event);};
G.cursor.down = function(event){G.cursor.setState("down");G.cursor.setPosition(event);};
G.cursor.darken = function(){
	G.cursor.img.alpha = 0.5;
};
G.cursor.lighten = function(){
	G.cursor.img.alpha = 1;
};

G.cursor.setPosition = function(event){
	//console.log("setting cursor pos");
	if(G.cursor.img){
		//console.log("cursor has img");
		G.cursor.img.x = event.stageX;
		G.cursor.img.y = event.stageY;
	}
};