G.dialogue_tree = {
	"sofia":{
		"emotion":"neutral",
		"utterance":"Hi",
		"choices":{
			"What's your name?":{
				"utterance":"Sofia...Sofia Sanchez",
				"choices":{
					"What do you do for a living, Ms. Sanchez?":{
						"utterance":"I just got a job as a waitress.",
						"action":{"back":1}
					},
					"Do you have any pets?":{
						"utterance":"No, but I used to have a dog. Why do you ask?",
						"choices":{
							"Oh, no reason.":{"action":{"back":2}},
						}
					}
				}
			},
			"What were you doing last night?":{
				"utterance":"Are you some kind of pervert?",
				"choices":{
					"No, ma'am.":{
						"utterance":"Just kidding with you! I threw a party last night.",
						"choices":{
							"What kind of party?":{
								"utterance":"The fun kind",
								"action":{"back":1}
							},
							"Where was the party?":{
								"utterance":"All over. Some of us were hanging out in the apartment. Some were in the garage",
								"choices":{
									"Can I see the garage?":{
										"utterance":"I wish. Lost my keys. Need to get a new set from the landlord.",
										"choices":{
											"That's a shame":{
												"action":{"back":3}
											},
											"I'll keep an eye out.":{
												"utterance":"Thanks!"
											}
										}
									}
								}
							},
							
						}
					}
				}
			},
			"May I see your apartment?":{
				"utterance":"I'm sorry, but I don't have the time. Late for work.",
				"choices":{
					"It will only take a minute.":{
						"utterance":"I'm sorry - I start my shift in a few minutes.",
						"action":{"condition":["mike_arrive", "sofia_leave"]}
					},
					"Where do you work?":{
						"utterance":"I'm sorry, but I've got to go.",
						"action":{"condition":["mike_arrive", "sofia_leave"]}
					}
				}
			}
		}
	},
	"jenny":{
		"emotion":"neutral",
		"utterance":"What can I do for you?",
		"choices":{
			"What's your name?":{
				"utterance":"Jenny",
				"action":{"back":1}
			},
			"Do you live here?":{
				"utterance":"Yes.",
				"choices":{
					"Do you own the building?":{
						"utterance":"No, that's Kevin.",
						"choices":{
							"May I see your apartment?":{
								"utterance":"Well, I'm in a rush to get to school. Sorry!",
								"action":{"condition":["mike_arrive", "jenny_leave"]}
							},
							"Do you have a key to the garage?":{
								"utterance":"I don't have a car. Only Sofia does.",
								"action":{"back":1}
							},
							"Do you have the landlord's number?":{
								"utterance":"Not on me. I have it written down somewhere.",
								"action":{"back":1}
							}
						}
					}
				}
			},
			"Where were you last night?":{
				"utterance":"I was at a party.",
				"choices":{
					"The party in the garage?":{
						"utterance":"Yeah.",
						"action":{"condition":["jenny_leave"]}
					}
				}
			}
		}
	},
	"mike":{
		"emotion":"neutral",
		"utterance":"who are you?",
		"choices":{
			"Who are YOU?":{
				"utterance":"mike anderson. i live here.",
				"choices":{
					"What time did you get home last night?":{
						"utterance":"around 4am. I went to a late night movie",
						"action":{"back":1}
					},
					"Can you let me into the garage?":{
						"utterance":"sorry I don't have the keys. Sofia has them.",
						"action":{"back":1}
					},
					"Can you let me into the apartment?":{
						"utterance":"um...sure no problem",
						"action":{"open":"living_room"}
					}
				}
			},
			"I'm a humane investigator":{
				"utterance":"i don't have any pets",
				"choices":{
					"How about roommates?":{
						"utterance":"Sofia used to have a dog. She loves them.",
						"action":{"back":1}
					},
					"Were you at the party last night?":{
						"utterance":"no. I didn't know they were having one.",
						"action":{"back":1}
					},
					"Can I ask you something else?":{
						"utterance":"i guess",
						"action":{"back":2}
					}
				}
			}
		}
	},
	"sofia_sit":{
		"emotion":"neutral",
		"utterance":"Oh, hello. Turns out I don't have to work today.",
		"choices":{
			"I'm afraid I do.":{
				"utterance":"Are you sure? You could just hang out.",
				"choices":{
					"Quite sure.":{
						"action":{"back":2}
					}
				}
			},
			"Perhaps you can answer some of my questions.":{
				"utterance":"Whatcha wanna know?",
				"choices":{
					"We got a complaint about a possible animal crime":{
						"utterance":"Did a penguin rob a bank?",
						"choices":{
							"Haha Very funny.":{}
						}
					},
					"Have you encountered any animals lately?":{
						"utterance":"You mean like squirrels?",
						"choices":{
							"I mean like dogs":{
								"utterance":"Uh...no."
							},
							"I think you know what I mean.":{
								"utterance":"I haven't the slightest."
							}
						}	
					},
					"You used to have a dog, is that right?":{
						"utterance":"Yes. Thanks for bringing up terrible memories."
					}
				}
			}
		}
	},
	"kevin_phone":{
		"utterance":"Hello?",
		"choices":{
			"Hello, is this Kevin Wu?":{
				"utterance":"Yes it is. Who is this?",
				"choices":{
					"I'm a humane investigator.":{
						"utterance":"What is this about?",
						"action":{"back":1}
					},
					"That's immaterial":{
						"utterance":"Well, then, I guess I can just hang up.",
						"action":{"back":1}
					},
					"I need you to come down to one of your properties.":{
						"utterance":"What's going on here?",
						"choices":{
							"A crime against an animal was committed here.":{
								"utterance":"I'll be right there.",
								"action":{"condition":["kevin_arrive"]}
							}
						}
					}
				}
			}
		}
	},
	"other_phone":{
		"utterance":"I think you have the wrong number"
	},
	"other1_phone":{
		"utterance":"Baby, I told you never to call me here!"
	},
	"other2_phone":{
		"utterance":"Oh, hello! Let me tell you about my cat, Prof. Perkins.",
		"choices":{
			"Uh, sorry. Wrong number.":{
				"action":{"end":1}
			}
		}
	},
	"kevin":{
		"utterance":"You'd better make this quick. I need to pick up my kids.",
		"choices":{
			"Open up the garage.  Quick enough for ya?":{
				"utterance":"Here you go. Leave them in the mailbox when you're done.",
				"action":{"condition":["kevin_leave"], "get":"keys"}
			},
			"May I please have access to the garage in the rear?":{
				"utterance":"What? I stopped listening halfway.",
				"action":{"back":1}
			}
		}
	}
}
