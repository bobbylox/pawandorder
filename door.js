G.doors = {};
G.doors.map = {};
G.doors.add = function(fromRoom, toRoom, shape, conditions){
	var door = new G.Door(toRoom, shape, conditions);
	G.doors.map[""+fromRoom+toRoom] = door;
	return door;
};
G.doors.getDoors = function(fromRoom, toRoom){
	if(toRoom){
		console.log(""+fromRoom+toRoom, G.doors.map, G.doors.map[""+fromRoom+toRoom]);
		var drs = [];
		drs.push(G.doors.map[""+fromRoom+toRoom]);
		return drs;
	}
	else{
		var doors = [];
		for(var d in G.doors.map){
			if(d.substr(0,fromRoom.length)==fromRoom){
				doors.push(G.doors.map[d]);
			}
		}
		return doors;
	}
};

G.Door = function(toRoom, shape, conditions){ //conditions are met when the door is passed through
	var that = this;
	this.children = [];
	this.lock = function(){
		that.locked = true;
		if(that.hotspot){
			that.removeChild(that.hotspot);
		}
		that.hotspot = new G.Hotspot(shape, G.nothing, "doorlocked");
		that.addChild(that.hotspot);
	}
	this.lock();
	this.nextRoom = function(){
		//console.log(toRoom);
		G.scenes.setScene(toRoom);
		for(var i in conditions){
			G.conditions.met(conditions[i]);
		}
	}
	this.unlock = function(){
		that.locked = true;
		if(that.hotspot){
			that.removeChild(that.hotspot);
		}
		that.hotspot = new G.Hotspot(shape, that.nextRoom, "doorunlocked");
		that.addChild(that.hotspot);
	}
};
G.Door.prototype = new createjs.Container();