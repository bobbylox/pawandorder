G.Examinable = function(w,h,name,messages,cursor,fn){
	this.children = [];
	var close;
	var shape = new createjs.Shape();
	shape.graphics.beginFill("#F00").dr(0,0,w, h);
	
	var img_names = ["normal", "fingerprint", "blood"];
	var alert = messages||{};
	var imgs = [];
	for(var i in img_names){
		var img_data = G.queue.getResult(name+"_"+img_names[i]);
		if(!img_data){
			if(!messages[img_names[i]]){
				messages[img_names[i]] = "No "+img_names[i]+" evidence found."
			}
			img_data = G.queue.getResult(name+"_normal");
		}
		imgs.push(new createjs.Bitmap(img_data));
	}
	var sceneBounds = G.scene.getBounds()||{width:1024, height:768};
	close = new G.Closeup(imgs[0], imgs[1], imgs[2], messages);
	G.scene.addChild(close);
	G.examinable = close;
		
	var zoom = function(){
		console.log("zoom!");
		if(cursor){
			if(G.cursor.name==cursor){
				(function(){
					console.log("special thing");
					fn();
				})();
			}
		}
		G.scene.addChild(close);
		close.show_normal();
		if(!G.inventory.deployed){
			G.inventory.toggleDeployed();
		}
		G.inventory.selectNormal();
		G.examinable = close;
	}
	
	this.close = close;
	var hotspot = new G.Hotspot(shape, zoom, "examine");
	this.addChild(hotspot);
};
G.Examinable.prototype = new createjs.Container();



G.Closeup = function(normal, fingerprint, blood, messages){
	this.children = [];
	var active = 0;
	var that = this;
	
	this.setNormal = function(normal){
		console.log("setting normal 0");
		that.normal = normal;
		that.normal.alpha = 0;
		that.addChild(that.normal);
		console.log("setting normal 1");
		var bounds = G.stage.getBounds();
		var closeupBounds = normal.getBounds?normal.getBounds():{width:1024, height:768};
		normal.x = (bounds.width-closeupBounds.width)*0.5;
		normal.y = (bounds.height-closeupBounds.height)*0.5;
		console.log("setting normal 2");
	};
	this.setFingerprint = function(fingerprint){
		that.fingerprint = fingerprint;
		fingerprint.alpha = 0;
		that.addChild(fingerprint);
		var bounds = G.stage.getBounds();
		var closeupBounds = fingerprint.getBounds?fingerprint.getBounds():{width:1024, height:768};
		fingerprint.x = (bounds.width-closeupBounds.width)*0.5;
		fingerprint.y = (bounds.height-closeupBounds.height)*0.5;
	};
	this.setBlood = function(blood){
		that.blood = blood;
		blood.alpha = 0;
		that.addChild(blood);
		var bounds = G.stage.getBounds();
		var closeupBounds = blood.getBounds?blood.getBounds():{width:1024, height:768};
		blood.x = (bounds.width-closeupBounds.width)*0.5;
		blood.y = (bounds.height-closeupBounds.height)*0.5;
	};
	this.setMessages = function(messages){
		that.messages = messages;
	};
	
	this.setNormal(normal);
	this.setFingerprint(fingerprint);
	this.setBlood(blood);
	this.setMessages(messages);
	
	var text = new createjs.Text("","36pt Helvetica", "#F00");
	this.addChild(text);
	
	
	this.show_normal = function(){
		console.log("show normal", this.normal, that);
		if(active==3){
			this.blood.alpha = 0;
		}
		else if(active==2){
			this.fingerprint.alpha = 0;
		}
		this.normal.alpha = 1;
		active = 1;
		createjs.Tween.get(that).to({alpha:1},1000);
		if(this.messages.normal){
			text.text = this.messages.normal;
		}
		else{
			text.text = "";
		}
	}
	
	this.show_fingerprint = function(){
		console.log("show fingerprints");
		if(active==3){
			this.blood.alpha = 0;
		}
		else if(active==1 && this.fingerprint){
			this.normal.alpha = 0;
		}
		this.fingerprint.alpha = 1;
		active = 2;
		createjs.Tween.get(that).to({alpha:1},1000);
		if(this.messages.fingerprint){
			text.text = this.messages.fingerprint;
		}
		else{
			text.text = "";
		}
	}
	
	this.show_blood = function(){
		console.log("show blood");
		if(active==2){
			this.fingerprint.alpha = 0;	
		}
		else if(active==1 && this.blood){
			this.normal.alpha = 0;
		}
		this.blood.alpha = 1;
		active = 3;
		createjs.Tween.get(that).to({alpha:1},1000);
		
		if(this.messages.blood){
			text.text = this.messages.blood;
		}
		else{
			text.text = "";
		}
	}
	
	this.disappear = function(event){
		function removeThis(){
			G.scene.removeChild(that);
		}
		//console.log("disappear!");
		createjs.Tween.get(that).to({alpha:0}, 500);//.call(removeThis);
		/*if(G.inventory.deployed){
			G.inventory.toggleDeployed();
		}*/
		G.examinable = null;
	};
	
	var shape = new createjs.Shape();
	var bounds = G.stage.getBounds();
	shape.graphics.beginFill("#000").dr(0,0, bounds.width, bounds.height);
	this.addChild(new G.Hotspot(shape, that.disappear));
}
G.Closeup.prototype = new createjs.Container();