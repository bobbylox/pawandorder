G.Hotspot = function(boundaryShape, handleClick, cursor){
	this.hitArea = boundaryShape;
	function clickFn(){
		//console.log("call function ",handleClick);
		var orig = G.cursor.name;
		handleClick();
		/*if(G.cursor.item && G.cursor.name==orig){
			G.inventory.add(G.cursor.name);
			G.cursor.setImage("default");
		}*/
	}
	this.addEventListener("pressup", clickFn);
	if(cursor){
		var changeCursor = function(){
			if(!G.cursor.item){
				G.cursor.setImage(cursor);
			}
		};
		//this.previousCursor = G.cursor.name;
		var changeBack = function(){
			//console.log("change back");
			if(!G.cursor.item){
				G.cursor.setImage(G.cursor.prevName);
			}
		}
		this.addEventListener("rollover", changeCursor);
		this.addEventListener("rollout", changeBack);
	}
}
G.Hotspot.prototype = new createjs.Shape();