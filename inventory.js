G.inventory = {};

G.inventory.init = function(){
	G.inventory.contents = [];
	G.inventory.map = {};
	G.inventory.display = new createjs.Container();
	G.inventory.deployed = false;
	
	//background
	var bg = new createjs.Bitmap(G.queue.getResult("inventory_bg"));
	bg.x = 0;
	//console.log(G.stage.getBounds());
	bg.y = -44;
	G.inventory.display.addChild(bg);
	
	//show/hide button
	var bgb = bg.getBounds();
	var buttonHit = new createjs.Shape();
	buttonHit.graphics.beginFill("#F00").lt(bgb.width-130,0).lt(bgb.width-86,-50).lt(bgb.width,-50).lt(bgb.width,0).cp().ef();
	G.inventory.button = new G.Hotspot(buttonHit, G.inventory.toggleDeployed);
	G.inventory.display.addChild(G.inventory.button);
	
	//event handler mask
	var mask = new createjs.Shape();
	mask.graphics.beginFill("#000").dr(0, 0, bgb.width, bgb.height);
	var mask_box = new G.Hotspot(mask, G.nothing);
	G.inventory.display.addChild(mask_box);
	
	var dockLocations = [454, 544, 634, 724, 814, 904];
	var dock;
	dockLocations.forEach(
		function(x){
			dock = new G.ItemDock(76, 76);
			dock.x = x;
			dock.y = 40;
			G.inventory.display.addChild(dock);
			G.inventory.contents.push(dock);
		}
	);
	G.inventory.display.y = 768;
	G.stage.addChild(G.inventory.display);
	
	//add the toggle buttons
	var each = [
		{
			"data":{
				"open":{
					"img":"view_normal_down",
					"x":0,   //pixel offsets
					"y":0
				},
				"close":{
					"img":"view_normal_up",
					"x":0,   //pixel offsets
					"y":0
				}
			},
			"onOpen":function(){if(G.examinable){G.examinable.show_normal();}}
		},
		{
			"data":{
				"open":{
					"img":"view_fingerprint_down",
					"x":120,   //pixel offsets
					"y":0
				},
				"close":{
					"img":"view_fingerprint_up",
					"x":120,   //pixel offsets
					"y":0
				}
			},
			"onOpen":function(){if(G.examinable){G.examinable.show_fingerprint();}}
		},
		{
			"data":{
				"open":{
					"img":"view_blood_down",
					"x":240,   //pixel offsets
					"y":0
				},
				"close":{
					"img":"view_blood_up",
					"x":240,   //pixel offsets
					"y":0
				}
			},
			"onOpen":function(){if(G.examinable){G.examinable.show_blood();}}
		}
	];
	G.inventory.examine_toggle = new G.MultiToggle(each);
	G.inventory.examine_toggle.x = 34;
	G.inventory.examine_toggle.y = 16;
	G.inventory.display.addChild(G.inventory.examine_toggle);
}

G.inventory.selectNormal = function(){
	G.inventory.examine_toggle.activate(0);
};

G.inventory.add = function(name){
	var added = false;
	for(var i in G.inventory.contents){
		if(!G.inventory.contents[i].occupant){
			G.inventory.contents[i].setOccupant(name);
			break;
		}
	}
};

G.inventory.remove = function(name){
	for(var i in G.inventory.contents){
		if(G.inventory.contents[i].occupant){
			if(G.inventory.contents[i].occupant.name==name){
				G.inventory.contents[i].popOccupant();
				return;
			}
		}
	}
};

G.inventory.toggleDone = function(){
	//dunno what to do here.  Nothing is probably fine.
}

G.inventory.toggleDeployed = function(event){
	//console.log("toggling state");
	if(G.inventory.deployed){
		createjs.Tween.get(G.inventory.display, {override:true}).to({y:768},500, createjs.Ease.quintInOut).call(G.inventory.toggleDone);
	}
	else{
		createjs.Tween.get(G.inventory.display, {override:true}).to({y:618},500, createjs.Ease.quintInOut).call(G.inventory.toggleDone);
	}

	G.inventory.deployed = !G.inventory.deployed;
}
