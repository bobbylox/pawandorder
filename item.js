G.items = {};
G.items.list = {};
G.items.remove = function(name){
	var item = G.items.list[name];
	item.parent.removeChild(item);
}
G.items.add = function(name){
	var item;
	if(G.items.list[name]){
		item = G.items.list[name];
	}
	else{
		item = new G.Item(name);
		G.items.list[name] = item;
	}
	return item;
}

G.Item = function(name){
	//Image
	this.children = [];
	var bitmap = new createjs.Bitmap(G.queue.getResult(name));
	this.addChild(bitmap);
	this.name = name;
	//HotSpot
	//bounds
	var bitBounds = bitmap.getBounds()
	if(!bitBounds){
		bitmap.setBounds(0,0,25,25);
		var bitBounds = bitmap.getBounds()
	}
	console.log("item", bitmap, bitBounds);
	var geom = new createjs.Shape();
	geom.graphics.beginFill("#F00").dr(0,0, bitBounds.width, bitBounds.height);
	
	//function
	var itemBehaviour = function(){
		G.cursor.setImage(name, true);
		//this.alpha = 0;
		G.items.remove(name);
	}
	this.hotspot = new G.Hotspot(geom, itemBehaviour, "grab");
	this.addChild(this.hotspot);
}
G.Item.prototype = new createjs.Container();

G.ItemDock = function(w, h, accepts){
	this.children = [];
	var that = this;
	this.occupant = null;
	this.accepts = accepts||{all:""};
	
	var hitBox = new createjs.Shape();
	hitBox.graphics.beginFill("#F00").dr(0,0,w,h);
	
	this.setOccupant = function(name){
		console.log("adding occupant", name);
		if("all" in that.accepts || name in that.accepts){
			var item = G.items.add(name);
			var bounds = item.getBounds();
			item.x = (w-bounds.width)*0.5;
			item.y = (h-bounds.height)*0.5;
			this.addChildAt(item,0);
			that.occupant = item;
		}
		if(name in that.accepts){
			eval(that.accepts[name]);
		}
	};
	
	this.popOccupant = function(){
		this.removeChild(this.occupant);
		var name = that.occupant.name;
		console.log("removing occupant", name);
		G.cursor.setImage(name, true);
		that.occupant = null;
		return name;
	};
	
	var dockBehaviour = function(event){
		if(G.cursor.item && !that.occupant){
			that.setOccupant(G.cursor.name);
			G.cursor.setImage("default");
		}
		else if(that.occupant && !G.cursor.item){
			that.popOccupant();
		}
	}
	this.hotspot = new G.Hotspot(hitBox, dockBehaviour);
	//console.log(this, this.addChild, this.hotspot);
	this.addChild(this.hotspot);
}
G.ItemDock.prototype = new createjs.Container();