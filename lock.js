G.Lock = function(shape, cursor, func, w, h){
	//when this area is clicked with a certain cursor, a function is called
	if(!shape){
		shape = new createjs.Shape();
		shape.graphics.beginFill("#000").drawRect(0,0,w||0,h||0);
	}
	function clickFn(){
		if(G.cursor.name==cursor){
			func();
		}
	}
	return new G.Hotspot(shape, clickFn, "question");
};