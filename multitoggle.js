G.MultiToggle = function(each){
	//data takes the form
	//[{"data":(see Toggle), "onOpen":function(){}, "onClosed":function(){}},...]
	this.children = [];
	this.which = null;
	var toggles = [];
	var self = this;
	
	function makeOpenFn(j){
		var info = each[j];
		var that = this;
		return function(){
			console.log("open",j);
			info["onOpen"]();
			if(that.which!=null){
				toggles[that.which].close_up();
			}
			that.which = j;
		};
	};
	function makeClosedFn(j){
		var info = each[j];
		var that = this;
		return function(){
			console.log("close",j);
			(info["onClosed"]||G.nothing)();
			if(that.which==j){
				that.which = null;
			}
		};
	};
	function makeToggler(obj,opener,closer) {
		return new G.Toggle(
		   obj.data,
		   opener,
		   closer
		);
	};
	
	for(var i=0; i<each.length; i++){
		(function(){
			var openFn = makeOpenFn(i);
			var closedFn = makeClosedFn(i);
			toggles[i] = makeToggler(each[i],openFn,closedFn);
			toggles[i].close_up();
			self.addChild(toggles[i]);
		})();
	}
	this.activate = function(number){
		console.log(number);
		for(var i in toggles){
			if(i==number){
				console.log("opening",i);
				toggles[i].open_up();
			}
			else{
				console.log("closing",i);
				toggles[i].close_up();
			}
		}
	};
	console.log(toggles);
}
G.MultiToggle.prototype = new createjs.Container();