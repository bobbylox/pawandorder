G.Phone = function(){
	//appearance
	this.children = [];
	var that = this;
	
	var bg_shape = new createjs.Shape();
	var screen_bounds = G.stage.getBounds();
	bg_shape.graphics.beginFill("#000").dr(0,0,screen_bounds.width, screen_bounds.height);
	var background = new G.Hotspot(bg_shape, function(){that.disappear();});
	this.addChild(background);
	
	var bg = new createjs.Bitmap(G.queue.getResult("phone"));
	bg.x = 246;
	bg.y = 175;
	var bg_bnds = bg.getBounds();
	var bg_shape = new createjs.Shape();
	bg_shape.graphics.beginFill("#000").dr(bg.x, bg.y, bg_bnds.width, bg_bnds.height);
	this.addChild(bg);
	this.addChild(new G.Hotspot(bg_shape, G.nothing));
	
	this.light = new createjs.Bitmap(G.queue.getResult("phone_light"));
	this.light.x = 618;
	this.light.y = 560;
	this.light.alpha = 0;
	this.addChild(this.light);
	
	this.dialed = "";
	
	//add all the main buttons
	var buttons = [["1","2","3"], ["4","5","6"], ["7","8","9"], ["*","0","#"]];
	var horizontal = 45;
	var vertical = 45;
	var top = 310;
	var left = 500;
	var overlay_data = G.queue.getResult("phone_button1");
	
	var makeButton = function(which){
		var btn = new createjs.Container();
		var img = new createjs.Bitmap(overlay_data);
		img.alpha = 0;
		
		var size = img.getBounds();
		var shape = new createjs.Shape();
		shape.graphics.beginFill("#000").dr(0,0,size.width, size.height);
		var hotspot = new G.Hotspot(shape, 
			function(){
				img.alpha = 1;
				createjs.Tween.get(img, {override:true}).to({alpha:0},300);
				that.dialed = that.dialed+which;
			}, 
			"grab");
		
		btn.addChild(hotspot);
		btn.addChild(img);
		
		return btn;
	}
	
	for(var i=0; i<buttons.length; i++){
		var row = buttons[i];
		for(var j=0; j<row.length; j++){
			var btn = makeButton(row[j]);
			btn.x = left+horizontal*j;
			btn.y = top+vertical*i;
			this.addChild(btn);
		}
	}
	
	//add the call button
	var overlay = new createjs.Bitmap(G.queue.getResult("phone_button2"));
	var bnds = overlay.getBounds();
	var shp = new createjs.Shape();
	shp.graphics.beginFill("#000").dr(0, 0, bnds.width, bnds.height);
	
	this.calling = false;
	var call_button = new G.Hotspot(shp, 
		function(){
			if(!that.calling){ //calling
				if(that.dialed.length<1){
					that.calling = true;
					that.light.alpha = 1;
					return;
				}
				
				if(that.dialed=="3126665900"||that.dialed=="13126665900"){ //calling kevin
					that.dialogue = new G.Dialogue.Whole("kevin_phone");
				}
				else if(that.dialed.charAt(0)<4){//wrong numbers
					that.dialogue = new G.Dialogue.Whole("other1_phone");
				}
				else if(that.dialed.charAt(0)<7){
					that.dialogue = new G.Dialogue.Whole("other2_phone");
				}
				else{ //calling anybody else
					that.dialogue = new G.Dialogue.Whole("other_phone");
				}
				that.dialogue.alpha = 1;
				G.stage.addChildAt(that.dialogue, G.stage.children.length-2);
				that.dialogue.say_next();
				that.calling = true;
				that.light.alpha = 1;
				that.dialed = "";
			}
			else{ //hanging up
				that.calling = false;
				that.light.alpha = 0;
				if(that.dialogue){
					that.dialogue.alpha = 0;
				}
			}
		},
		"grab");
	call_button.x = left;
	call_button.y = 490;
	this.addChild(call_button);
	
	this.disappear = function(){
		that.dialed="";
		console.log("disappear");
		createjs.Tween.get(that).to({alpha:0.0},800);
	}
};
G.Phone.prototype = new createjs.Container();