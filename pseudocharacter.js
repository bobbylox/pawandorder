G.PseudoCharacter = function(name){
	this.children = [];
	this.layers = {};
	this.name = name;
	var that = this;
	var spec = G.character_spec[name];
	
	//Hook for later removal
	G.conditions.set(name+"_leave", 
		function(){
			createjs.Tween.get(that).to({alpha:0}, 1000);
		}
	);
	
	//Background Image
	var bg = new createjs.Bitmap(G.queue.getResult(spec.bg));
	that.addChild(bg);
	
	//bounds
	var bitBounds = bg.getBounds();
	var geom = new createjs.Shape();
	geom.graphics.beginFill("#F00").dr(0,0,bitBounds.width, bitBounds.height);
	
	//animations
	if(spec.layers){
		var layerspec;
		var layer;
		for(var i in spec.layers){
			layerspec = spec.layers[i];
			if(layerspec.type=="bitmap"){
				layer = new createjs.Bitmap(G.queue.getResult(layerspec.id));
			}
			else if(layerspec.type=="toggle"){
				layer = new G.Toggle(layerspec.data);//maybe specify conditions
			}
			else if(layerspec.type=="examinable"){
				layer = new G.Examinable(layerspec.w,layerspec.h,layerspec.id,{});
			}
			else if(layerspec.type=="sprite"){
				var sheet = new createjs.SpriteSheet(layerspec.data);
				layer = new createjs.Sprite(sheet, "animate");
			}
			//this should be valid for all layer types
			this.layers[layerspec.id] = layer;
			layer.x = layerspec.x||0;
			layer.y = layerspec.y||0;
			this.addChild(layer);
		}
	}
	
	//bounds
	var bitBounds = bg.getBounds();
	var geom = new createjs.Shape();
	geom.graphics.beginFill("#F00").dr(0,0,bitBounds.width, bitBounds.height);
	
	//function
	this.accuse = function(){
		G.scenes.setScene("finale");
	};
	
	this.hotspot = new G.Hotspot(geom, this.accuse, "handcuff");
	this.addChild(this.hotspot);
};
G.PseudoCharacter.prototype = new createjs.Container();