Paw & Order: Furry Victims Unit

A point-and-click adventure game where you play as a humane investigator, investigating an animal crime.

Some instructions regarding dialogue notation:

utterance - 
What the character says

choices - 
What the player might say

action - 
What happens (besides advancing down the dialogue tree) when you reach this point?  There are several types of actions programmed as of now, and I can imagine you might need more.  If you invent new action types, just describe them to me to the right of double slashes

//this is a comment

action types: 

"back": #
this tells the dialogue manager to go back up the hierarchy of the dialog tree.  if # is 1, you basically freeze in place, if you go back 2, that's the previous set of choices, etc.

"end":1
this ends the dialogue.

"open":"room_name"
this opens the door to the specified room.

"condition":"condition_name"
this is kind of a catch-all for making something happen.  Telling it what is supposed to happen when this condition is met occurs elsewhere.
