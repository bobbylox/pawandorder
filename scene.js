//Scene

G.scenes = {};
G.scenes.map = {};
G.scenes.setScene = function(name){
	var new_scene;
	if(G.scenes.map[name]){
		new_scene = G.scenes.map[name];
	}
	else{
		new_scene = new G.Scene(name);
		G.scenes.map[name] = new_scene;
	}
	new_scene.alpha = 0;
	new_scene.x = 0;
	new_scene.y = 0;
	G.stage.addChildAt(new_scene, Math.max(0,G.stage.children.length-2));//the cursor, inventory are in front
	function removeOld(){
		if(G.scene){
			G.stage.removeChild(G.scene);
		}
		G.scene = new_scene;//G.scene is the current scene
		//console.log(G.scene);
	}
	if(G.timer){
		new_scene.addChild(G.timer);
	}
	else if(name !="building" && name != "intro"){
		G.scenes.startTimer();
	}
	createjs.Tween.get(new_scene, {override:true}).to({alpha:1}, 1400).call(removeOld);
	//console.log(G.scenes.map);
};
G.scenes.startTimer = function(){
	if(!G.timer){
		G.timer = new G.Timer(900, function(){G.scenes.setScene("accusations");});
		G.timer.x = 800;
		G.timer.y = 10;
	}
	G.scene.addChild(G.timer);
	G.timer.alpha = 1;
};

G.Scene = function(name){
	var that = this;
	this.layers = {};
	this.children = [];
	this.name = name;
	var spec = G.scene_spec[name];
	var bounds = G.stage.getBounds();
	this.width = bounds.width;
	this.height = bounds.height;
	
	//Background
	if(spec.bg.charAt(0)==="#"){
		this.bg = new createjs.Shape();
		this.bg.graphics.beginFill(spec.bg).dr(0,0,this.width, this.height);
	}
	else{
		this.bg = new createjs.Bitmap(G.queue.getResult(spec.bg));
	}
	this.addChild(this.bg);
	
	var arrive = function(layer){
		return function(){
			createjs.Tween.get(layer).to({alpha:1}, 1000);
		}
	}

	if(spec.layers){
		var layerspec;
		var layer;
		for(var i in spec.layers){
			layerspec = spec.layers[i];
			
			//All the layer types:
			if(layerspec.type=="bitmap"){
				layer = new createjs.Container();
				layer.addChild(new createjs.Bitmap(G.queue.getResult(layerspec.img||layerspec.id)));
			}
			if(layerspec.type=="item"){
				layer = G.items.add(layerspec.id);
			}
			else if(layerspec.type=="toggle"){
				var onOpen = layerspec.onOpen;
				if(!onOpen){
					onOpen = G.nothing;
				}
				else if(typeof onOpen==="string"){
					onOpen = G.conditions.stringToFn(onOpen);
				}
				
				var onClosed = layerspec.onClosed;
				if(!onClosed){
					onClosed = G.nothing;
				}
				else if(typeof onClosed==="string"){
					onClosed = G.conditions.stringToFn(onClosed);
				}
				console.log("onOpen",onOpen);
				layer = new G.Toggle(layerspec.data, onOpen, onClosed);
			}
			else if(layerspec.type=="examinable"){
				layer = new G.Examinable(
					layerspec.w,
					layerspec.h,
					layerspec.id,
					layerspec.messages||{}, 
					layerspec.cursor, 
					(typeof layerspec.fn)==="string"?G.conditions.stringToFn(layerspec.fn):layerspec.fn
					);
			}
			else if(layerspec.type=="sprite"){
				var sheet = new createjs.SpriteSheet(layerspec.data);
				layer = new createjs.Sprite(sheet, "animate");
			}
			else if(layerspec.type=="character"){
				layer = new G.Character(layerspec.id);
			}
			else if(layerspec.type=="pseudocharacter"){
				layer = new G.PseudoCharacter(layerspec.id);
			}
			else if(layerspec.type=="lock"){
				layer = new G.Lock(null, layerspec.cursor, layerspec.fn, layerspec.w, layerspec.h);
			}
			else if(layerspec.type=="hotspot"){
				var shape = new createjs.Shape();
				shape.graphics.beginFill("#000").dr(0,0,layerspec.w, layerspec.h);
				
				var fn = layerspec.fn;
				if(typeof fn==='string'){
					fn = G.conditions.stringToFn(fn);
				}
				layer = new G.Hotspot(shape, fn, layerspec.cursor);
			}
			else if(layerspec.type=="phone"){
				layer = new G.Phone();
			}
			else if(layerspec.type=="typewriter"){
				var onComplete = layerspec.onComplete;
				if(typeof onComplete === 'string'){
					onComplete = g.conditions.stringToFn(onComplete);
				}
				layer = new G.Typewriter(layerspec.text, layerspec.font, 
					layerspec.width, layerspec.height, 
					layerspec.time, layerspec.delay, 
					onComplete);
			}
			
			if(layerspec.condition){
				layer.alpha = 0;
				G.conditions.set(layerspec["condition"],
					arrive(layer)
				);
			}
			//this should be valid for all layer types
			this.layers[layerspec.id] = layer;
			layer.x = layerspec.x||0;
			layer.y = layerspec.y||0;
			if(layerspec.parent){
				this.layers[layerspec.parent].addChild(layer);
			}
			else{
				this.addChild(layer);
			}
		}
	}
	if(spec.doors){
		var door;
		var doorshape;
		var doorspec;
		for(var d in spec.doors){
			doorspec = spec.doors[d];
			doorshape = new createjs.Shape();
			doorshape.graphics.beginFill("#F00").dr(0,0,doorspec.w,doorspec.h);
			var conds = undefined;
			if(doorspec.condition){
				conds = doorspec.condition;
			}
			door = G.doors.add(name,d,doorshape, conds);
			door.x = doorspec.x || 0;
			door.y = doorspec.y || 0;
			that.addChild(door);
			if(doorspec.unlocked){
				door.unlock();
			}
		}
	}
}
G.Scene.prototype = new createjs.Container();

//Condition System

G.conditions = {};
G.conditions.map = {};
G.conditions.met_list = {};
G.conditions.set = function(cond, func){
	G.conditions.map[cond] = func;
};
G.conditions.setMultiple = function(conds, func){
	var multicond = "";
	for(var c in conds){
		multicond+=conds[c];
		if(c<conds.length-1){
			multicond+="&&";
		}
	}
	G.conditions.set(multicond, func);
};
G.conditions.met = function(cond){
	G.conditions.met_list[cond] = true;
	console.log("met condition", cond);
	if(G.conditions.map[cond]){
		G.conditions.map[cond]();
	}
	else{
		for(var c in G.conditions.map){
			if(c.indexOf(cond)>=0){
				var conds = c.split("&&"); //get the list of all conditions which must be met
				var all_met = true; 
				var other;
				var i = 0;
				while(all_met && i<conds.length){//verify each is met
					other = conds[i];
					all_met = all_met && G.conditions.met_list[other];
					i++;
				}
				if(all_met){
					G.conditions.map[c]();
				}
			}
		}
	}
};
G.conditions.stringToFn = function(str){
	return function condMet(){
		G.conditions.met(str);
	};
};