G.scene_spec = {
	"intro":{
		"bg":"#FFF",
		"layers":[
			{
				"id":"intro text",
				"type":"typewriter",
				"text":"In the Animal Justice System,\n Humane Investigators are\n trained to investigate\n abuse and neglect of animals.  \n\n   These are their stories.",
				"x":15,
				"y":100,
				"font":"42pt Courier",
				"onComplete":function(){G.conditions.met("logo_appear");G.doors.getDoors("intro","building")[0].unlock();G.scenes.startTimer();}
			},
			{
				"id":"logo",
				"type":"bitmap",
				"condition":"logo_appear"
			}
		],
		"doors":{
			"building":{
				//"unlocked":true,//DEBUG
				"w":1024,
				"h":768
			}
		}
	},
	
	"building":{
		"bg":"building",
		"layers":[
			{
				"id":"fan",
				"type":"sprite",
				"data":{
					"images": ["assets/img/background/Env_Building_Fan.png"],
					"framerate":10,
					"frames": [
						[2, 2, 40, 42], 
						[44, 2, 40, 42]
					],
					"animations": {
							"animate":[0,1]
					}
				},
				"x":540,
				"y":280
			},
			{
				"id":"tree",
				"type":"sprite",
				"data":{
					"images": ["assets/img/background/Env_anim_tree.png"],
					"frames": [
						[2, 2, 211, 255], 
						[215, 2, 211, 255], 
						[2, 259, 211, 255], 
						[215, 259, 211, 255], 
						[2, 516, 211, 255], 
						[215, 516, 211, 255], 
						[2, 773, 211, 255], 
						[215, 773, 211, 255], 
						[2, 1030, 211, 255], 
						[2, 1287, 211, 255], 
						[215, 1030, 211, 255], 
						[215, 1287, 211, 255]
					],
					"animations": {
						"animate":{
							"frames":[0,1,2,3,4,5,6,7,8,9,10,11],
							"speed":0.5
						}
					}
				}
			},
			{
				"id":"shadow",
				"type":"sprite",
				"x":0,
				"y":498,
				"data":{
					"images": ["assets/img/background/Env_anim_shadow.png"],
					"frames": [

						[2, 2, 351, 94], 
						[2, 98, 351, 94], 
						[355, 2, 351, 94], 
						[708, 2, 351, 94], 
						[355, 98, 351, 94], 
						[708, 98, 351, 94]
					],
					"animations": {
						"animate":{
							"frames":[0,1,2,3,4,5,5,4,3,2,1,0],
							"speed":0.5
						}
					}
				}
			},
			{
				"id":"sofia",
				"type":"character",
				"x":530,
				"y":300
			},
			{
				"id":"jenny",
				"type":"character",
				"x":640,
				"y":300
			},
			{
				"id":"mike",
				"type":"character",
				"x":240,
				"y":295,
				"condition":"mike_arrive"
			},
			{
				"id":"kevin",
				"type":"character",
				"x":360,
				"y":300,
				"condition":"kevin_arrive"
			},
			{
				"id":"puddle",
				"type":"lock",
				"cursor":"boot",
				"fn":function(){G.cursor.setImage("boot_mud",true);},
				"x":825,
				"y":400,
				"w":150,
				"h":145
			},
			{
				"id":"mission",
				"type":"typewriter",
				"text":"An anonymous report of \npossible animal abuse \njust came in.\nGo check it out.\nYou've got 15 minutes.",
				"x":160,
				"y":15,
				"w":500,
				"h":200,
				"font":"36pt courier",
				"onComplete":function(){createjs.Tween.get(G.scenes.map.building.layers.mission).to({alpha:0}, 1000).call(G.scenes.startTimer);}
			}
		],
		"doors":{
			"living_room":{
				//"unlocked":true,//DEBUG
				"x":700,
				"y":300,
				"w":100,
				"h":200
			},
			"alley":{
				"unlocked":true,
				"x":850,
				"y":300,
				"w":100,
				"h":100
			},
			"neighbor":{//this door goes nowhere, but that's OK
				"x":50,
				"y":300,
				"w":100,
				"h":200
			}
		}
	},
	"living_room":{
		"bg":"living_room",
		"layers":[
			{
				"id":"tv1",
				"type":"sprite",
				"x":860,
				"y":206,
				"data":{
					"images": ["assets/img/background/Env_anim_tv1.png"],
					"frames": [

						[2, 2, 93, 60], 
						[97, 2, 93, 60], 
						[192, 2, 93, 60], 
						[287, 2, 93, 60], 
						[382, 2, 93, 60]
					],
					"animations": {
						"animate":{
							"frames":[0,0,0,0,1,2,3,4],
							"speed":0.125
						}
					}
				}
			},
			{
				"id":"tv2",
				"type":"sprite",
				"x":860,
				"y":251,
				"data":{
					"images": ["assets/img/background/Env_anim_tv2.png"],
					"frames": [
						[2, 2, 93, 64], 
						[2, 68, 93, 64], 
						[2, 134, 93, 64], 
						[2, 200, 93, 64], 
						[2, 266, 93, 64], 
						[2, 332, 93, 64]
					],
					"animations": {
						"animate":{
							"frames":[0,1,2,3,4,5],
							"speed":0.25
						}
					}
				}
			},
			{
				"id":"sofia_sit",
				"type":"character",
				"x":322,
				"y":227,
				"condition":"sofia_livingroom"
			},
			{
				"id":"boot",
				"type":"item",
				"x":400,
				"y":550,
				"condition":"boot_appear"
			},
			{
				"id":"paper",
				"type":"bitmap",
				"x":234,
				"y":350
			},
			{
				"id":"paper_cu",
				"type":"examinable",
				"x":224,
				"y":340,
				"w":70,
				"h":46,
				"lock":true,
				"cursor":"boot_mud",
				"fn":function(){
					var new_cu = new createjs.Bitmap(G.queue.getResult("paper_cu_bootprint"));
					console.log("swapping cu", new_cu, G.scene.layers.paper_cu.close);
					G.scene.layers.paper_cu.close.setNormal(new_cu);
					G.scene.layers.paper_cu.close.setFingerprint(new_cu);
					G.scene.layers.paper_cu.close.setBlood(new_cu);
				}
			}
		],
		"doors":{
			"building":{
				"unlocked":true,
				"x":900,
				"y":200,
				"w":124,
				"h":400
			},
			"kitchen":{
				"unlocked":true,
				"x":0,
				"y":0,
				"w":100,
				"h":768
			}
		}
	},
	"kitchen":{
		"bg":"kitchen",
		"layers":[
			{
				"id":"drip",
				"type":"sprite",
				"x":895,
				"y":312,
				"data":{
					"images": ["assets/img/background/Env_anim_drip.png"],
					"frames": [
						[2, 2, 6, 40], 
						[10, 2, 6, 40], 
						[18, 2, 6, 40], 
						[2, 44, 6, 40], 
						[2, 86, 6, 40], 
						[10, 44, 6, 40], 
						[10, 86, 6, 40]
					],
					"animations": {
						"animate":{
							"frames":[0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6], 
							"speed":0.5
						}
					}
				}
			},
			{
				"id":"microwave_timer",
				"type":"sprite",
				"x":605,
				"y":100,
				"data":{
					"images": ["assets/img/background/Env_anim_time.png"],
					"frames": [

						[2, 2, 24, 13], 
						[2, 17, 24, 13]
					],
					"animations": {
						"animate":{
							"frames":[0,1], 
							"speed":0.05
						}
					}
				}
			},
			{
				"id":"cereal",
				"type":"examinable",
				"messages":{
					"fingerprint":"Fingerprint match: Mike Anderson"
				},
				"x":150,
				"y":80,
				"w":50,
				"h":50
			},
			{
				"id":"kevins_number",
				"type":"examinable",
				"x":130,
				"y":160,
				"w":50,
				"h":50
			},
			{
				"id":"phone_zone",
				"type":"hotspot",
				"cursor":"examine",
				"fn":"show_phone",
				"x":930,
				"y":200,
				"w":60,
				"h":80
			},
			{
				"id":"phone",
				"type":"phone",
				"condition":"show_phone"
			}
		],
		"doors":{
			"living_room":{
				"unlocked":true,
				"x":924,
				"y":300,
				"w":100,
				"h":468,
				"condition":["sofia_leave", "sofia_livingroom","boot_appear"]
			}
		}
	},
	"alley":{
		"bg":"alley",
		"layers":[
			{
				"id":"plant1",
				"type":"sprite",
				"data":{
					"images": ["assets/img/background/Env_anim_plant1.png"],
					"framerate":10,
					"frames": [
						[2, 2, 46, 67], 
						[2, 71, 46, 67], 
						[2, 140, 46, 67], 
						[2, 209, 46, 67], 
						[2, 278, 46, 67], 
						[2, 347, 46, 67], 
						[2, 416, 46, 67], 
						[2, 485, 46, 67], 
						[2, 554, 46, 67], 
						[2, 623, 46, 67], 
						[2, 692, 46, 67], 
						[2, 761, 46, 67]
					],
					"animations": {
						"animate":{
							"frames":[0,1,2,3,4,5,6,7,8,9,10,11],
							"speed":0.5
						}
					}
				},
				"x":269,
				"y":439
			},
			{
				"id":"plant2",
				"type":"sprite",
				"data":{
					"images": ["assets/img/background/Env_anim_plant2.png"],
					"frames": [

						[2, 2, 70, 109], 
						[74, 2, 70, 109], 
						[146, 2, 70, 109], 
						[218, 2, 70, 109], 
						[290, 2, 70, 109], 
						[362, 2, 70, 109], 
						[434, 2, 70, 109], 
						[506, 2, 70, 109], 
						[578, 2, 70, 109], 
						[650, 2, 70, 109], 
						[722, 2, 70, 109], 
						[794, 2, 70, 109]
					],
					"animations": {
							"animate":{
								"frames":[0,1,2,3,4,5,6,7,8,9,10,11],
								"speed":0.5
							}
					}
				},
				"x":657,
				"y":394
			},
			{
				"id":"rag_small",
				"img":"rag",
				"type":"bitmap",
				"x":910,
				"y":505,
				"condition":"lid_open"
			},
			{
				"id":"rag",
				"type":"examinable",
				"messages":{
					"blood":"Dog blood detected."
				},
				"parent":"rag_small",
				"w":40,
				"h":40
			},
			{
				"id":"garbage_lid",
				"type":"toggle",
				"data":{
					"open":{
						"img":"garbage_lid_open"
					},
					"close":{
						"img":"garbage_lid_closed"
					}
				},
				"onOpen":"lid_open",
				"x":825,
				"y":435
			},
			{
				"id":"alley_garagedoor_open",
				"type":"bitmap",
				"x":0,
				"y":319
			},
			{
				"id":"alley_garagedoor_closed",
				"type":"bitmap",
				"x":0,
				"y":319
			},
			{
				"id":"beer",
				"type":"examinable",
				"x":740,
				"y":550,
				"w":50,
				"h":75,
				"messages":{
					"fingerprint":"Fingerprint match: Mike Anderson"
				}
			}
		],
		"doors":{
			"building":{
				"unlocked":true,
				"x":412,
				"y":568,
				"w":200,
				"h":200
			},
			"garage":{
				"unlocked":true,
				"x":0,
				"y":100,
				"w":150,
				"h":500
			}
		}
	},
	"garage":{
		"bg":"garage",
		"layers":[
			{
				"type":"sprite",
				"id":"dust",
				"x":500,
				"y":150,
				"data":{
					"images": ["assets/img/background/Env_anim_dust.png"],
					"frames": [
						[2, 2, 205, 188], 
						[2, 192, 205, 188], 
						[2, 382, 205, 188], 
						[209, 2, 205, 188], 
						[209, 192, 205, 188], 
						[209, 382, 205, 188], 
						[416, 2, 205, 188], 
						[416, 192, 205, 188], 
						[416, 382, 205, 188], 
						[623, 2, 205, 188], 
						[623, 192, 205, 188], 
						[623, 382, 205, 188], 
						[830, 2, 205, 188], 
						[830, 192, 205, 188], 
						[830, 382, 205, 188], 
						[1037, 2, 205, 188], 
						[1037, 192, 205, 188], 
						[1037, 382, 205, 188], 
						[1244, 2, 205, 188], 
						[1451, 2, 205, 188], 
						[1244, 192, 205, 188], 
						[1451, 192, 205, 188], 
						[1244, 382, 205, 188], 
						[1451, 382, 205, 188]
					],
					"animations": {
							"animate":{
								"frames":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
								"speed":0.8
							}
					}
				}
			},
			{
				"id":"cup",
				"type":"examinable",
				"x":600,
				"y":460,
				"w":80,
				"h":80,
				"messages":{
					"fingerprint":"Fingerprint match: Sofia Sanchez"
				}
			},
			{
				"id":"footprint",
				"type":"examinable",
				"x":390,
				"y":500,
				"w":80,
				"h":80
			},
			{
				"type":"bitmap",
				"id":"garage_door"
			},
			{
				"type":"bitmap",
				"id":"garage_front"
			},
			{
				"id":"garagedoor_lock",
				"type":"lock",
				"cursor":"keys",
				"fn":function(){
					createjs.Tween.get(G.scene.layers.garage_door).to({y:-500},5000);
					G.scenes.map.alley.layers.alley_garagedoor_closed.alpha = 0;
				},
				"x":465,
				"y":560,
				"w":75,
				"h":75,
				"parent":"garage_door"
			}
		],
		"doors":{
			"alley":{
				"unlocked":true,
				"x":0,
				"y":700,
				"w":1024,
				"h":68
			}
		}
	},
	"accusations":{
		"bg":"#1FF",
		"layers":[
			{
				"id":"mike",
				"type":"pseudocharacter",
				"x":240,
				"y":295
			},
			{
				"id":"kevin",
				"type":"pseudocharacter",
				"x":360,
				"y":300
			},
			{
				"id":"sofia",
				"type":"pseudocharacter",
				"x":530,
				"y":300
			},
			{
				"id":"jenny",
				"type":"pseudocharacter",
				"x":640,
				"y":300
			},
			{
				"id":"question",
				"type":"typewriter",
				"text":"Who is guilty of Animal Cruelty?",
				"x":30,
				"y":180
			}
		]
	},
	"finale":{
		"bg":"#2EE",
		"layers":[
			{
				"id":"sofia",
				"type":"pseudocharacter",
				"x":530,
				"y":300
			},
			{
				"id":"final_text",
				"type":"typewriter",
				"font":"32pt Courier",
				"x":100,
				"y":100,
				"text":"Sofia Sanchez was running a\ndogfighting ring out\nof her garage.\n\nJenny knew, but chose to\ndo nothing.\n                        ",
				"onComplete":function(){G.scenes.setScene("credits");}
			}
		]
	},
	"credits":{
		"bg":"#3DD",
		"layers":[
			{
				"id":"acknowledgements",
				"type":"typewriter",
				"text":"\n\nTHE END.\n\nPaw & Order is a collaboration\nbetween Important Little Games\nThe Anti-Cruelty Society\nChicago Quest School and\nAfter School Matters.\n\nBrought to you through a\ngenerous grant from\nHive Chicago.",
				"x":15,
				"y":180,
				"font":"42pt Courier",
				"onComplete":function(){G.conditions.met("show_students");createjs.Tween.get(G.scene.layers.acknowledgements).to({alpha:0},1000);},
				"time":8000
			},
			{
				"id":"students",
				"type":"typewriter",
				"text":"Episode 1 Written By:\n\nCharlie Walker, Edman Alicea,\nGabe Nepstad, Jesse Lane,\nJiamon London, Lexy Paramo,\nMarcelo Wright, Michael Gunn,\nMichelle Martinez & Tionne Smith",
				"x":15,
				"y":180,
				"font":"36pt Courier",
				"onComplete":function(){G.conditions.met("show_mentors");createjs.Tween.get(G.scene.layers.students).to({alpha:0},1000);},
				"condition":"show_students",
				"delay":9000,
				"time":10000
			},
			{
				"id":"mentors",
				"type":"typewriter",
				"text":"Mentored by\n\n Jessica Coronado, Richard Costas,\nJalen Crump, Gesselle Garcia,\nLauryn Garner, Jordan Hunt,\nAndre Joe, Dyani Keelen,\nJovanni Morales, Jadiea Muldrew,\nTania Palafox, Maria Perez,\nRiley Presnell, Odaliz Quiles,\nHenry Quizhpi, Juanita Rodriguez,\nSavannah Rosario & Yasmine Russell",
				"x":15,
				"y":180,
				"font":"32pt Courier",
				"onComplete":function(){G.conditions.met("show_coordinators");createjs.Tween.get(G.scene.layers.mentors).to({alpha:0},1000);},
				"condition":"show_mentors",
				"delay":20000,
				"time":8000
			},
			{
				"id":"coordinators",
				"type":"typewriter",
				"text":"Program Coordinator.................Cat Greim\n\nProgram Director..............Elliott Serrano\n\nGame Developer...................Rob Lockhart\n\n\nSpecial Thanks To:\n\nConsulting Investigator...Dotty Cowles-Newton\n\nConsulting Veterinarian.....Dr. Adam W. Stern\n                                 ",
				"x":50,
				"y":180,
				"font":"24pt Courier",
				"condition":"show_coordinators",
				"delay":29000,
				"time":10000,
				"onComplete":function(){G.scenes.setScene("redeem")}
			}
		]
	},
	"redeem":{
		"bg":"#4CC"
	}
};