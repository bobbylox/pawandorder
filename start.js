var G = {};

G.progress = function(event){
	var logo_bounds = {width:238, height:119};
	console.log(Math.floor(event.progress*100));
	G.progressbar.style.width = Math.floor(event.progress*100)+"%";
};
    	
G.init = function() {
	console.log("INIT");
	//engine setup
	G.stage = new createjs.Stage("projectCanvas");
	G.stage.setBounds(0,0,1024,768);
	G.stage.enableMouseOver();
	createjs.Touch.enable(G.stage);
	
	G.queue = new createjs.LoadQueue(false);
	G.queue.installPlugin(createjs.Sound);
	G.queue.addEventListener("complete", G.ready);
	//progress bar
	G.logo = document.getElementById("logo");
	
	G.progressbar = document.getElementById("progressBar");

	G.queue.addEventListener("progress", G.progress);
	
	G.queue.loadManifest(
		[	
			//Logo
			{id:"logo",src:"assets/img/layer/logo.jpg"},
			//Backgrounds
			{id:"building",src:"assets/img/background/Env_Building.png"},
			{id:"building_fan",src:"assets/img/background/Env_Building_Fan.png"},
			{id:"living_room",src:"assets/img/background/Env_Livingroom.png"},
			{id:"kitchen",src:"assets/img/background/Env_Kitchen.png"},
			{id:"alley",src:"assets/img/background/Env_Alley.png"},
			{id:"garage",src:"assets/img/background/Env_Garage.png"},
			//layers
			{id:"garage_door",src:"assets/img/layer/Env_Garage_door.png"},
			{id:"garage_front",src:"assets/img/layer/Env_Garage_front.png"},
			{id:"garbage_lid_open",src:"assets/img/layer/lid_open.png"},
			{id:"garbage_lid_closed",src:"assets/img/layer/lid_closed.png"},
			{id:"alley_garagedoor_open", src:"assets/img/layer/alley_garagedoor_open.png"},
			{id:"alley_garagedoor_closed", src:"assets/img/layer/alley_garagedoor_closed.png"},
			//evidence
			{id:"beer_normal", src:"assets/img/item/beer_normal.png"},
			{id:"beer_fingerprint", src:"assets/img/item/beer_fingerprint.png"},
			{id:"cereal_normal", src:"assets/img/item/cereal_normal.png"},
			{id:"cereal_fingerprint", src:"assets/img/item/cereal_fingerprint.png"},
			{id:"kevins_number_normal", src:"assets/img/item/kevins_number.png"},
			{id:"rag", src:"assets/img/item/rag_small.png"},
			{id:"rag_normal", src:"assets/img/item/rag_normal.png"},
			{id:"rag_blood", src:"assets/img/item/rag_blood.png"},
			{id:"cup_normal", src:"assets/img/item/cup_normal.png"},
			{id:"cup_fingerprint", src:"assets/img/item/cup_fingerprint.png"},
			{id:"footprint_normal", src:"assets/img/item/footprint.png"},
			{id:"paper", src:"assets/img/item/paper.png"},
			{id:"paper_cu_normal", src:"assets/img/item/paper_cu_normal.png"},
			{id:"paper_cu_bootprint", src:"assets/img/item/paper_cu_bootprint_normal.png"},
			//phone
			{id:"phone", src:"assets/img/item/phone.png"},
			{id:"phone_light", src:"assets/img/item/phone_light.png"},
			{id:"phone_button1", src:"assets/img/item/phone_button1.png"},
			{id:"phone_button2", src:"assets/img/item/phone_button2.png"},
			//UI
			{id:"inventory_bg",src:"assets/img/ui/inventory_bg.png"},
			{id:"view_normal_up",src:"assets/img/ui/view_normal_up.png"},
			{id:"view_normal_down",src:"assets/img/ui/view_normal_down.png"},
			{id:"view_fingerprint_up",src:"assets/img/ui/view_fingerprint_up.png"},
			{id:"view_fingerprint_down",src:"assets/img/ui/view_fingerprint_down.png"},
			{id:"view_blood_up",src:"assets/img/ui/view_blood_up.png"},
			{id:"view_blood_down",src:"assets/img/ui/view_blood_down.png"},
			//cursors - these MUST start with the word cursor, and end with either up or down
			{id:"cursordefaultup",src:"assets/img/cursor/defaultup.png"},
			{id:"cursordefaultdown",src:"assets/img/cursor/defaultdown.png"},
			{id:"cursorgrabup",src:"assets/img/cursor/grabup.png"},
			{id:"cursorgrabdown",src:"assets/img/cursor/grabdown.png"},
			{id:"cursortalkup",src:"assets/img/cursor/talkup.png"},
			{id:"cursortalkdown",src:"assets/img/cursor/talkdown.png"},
			{id:"cursordoorlockedup",src:"assets/img/cursor/doorlockedup.png"},
			{id:"cursordoorlockeddown",src:"assets/img/cursor/doorlockeddown.png"},
			{id:"cursordoorunlockedup",src:"assets/img/cursor/doorunlockedup.png"},
			{id:"cursordoorunlockeddown",src:"assets/img/cursor/doorunlockeddown.png"},
			{id:"cursorexamineup",src:"assets/img/cursor/examineup.png"},
			{id:"cursorexaminedown",src:"assets/img/cursor/examinedown.png"},
			{id:"cursorquestionup",src:"assets/img/cursor/questionup.png"},
			{id:"cursorquestiondown",src:"assets/img/cursor/questiondown.png"},
			{id:"cursorhandcuffup",src:"assets/img/cursor/handcuffup.png"},
			{id:"cursorhandcuffdown",src:"assets/img/cursor/handcuffdown.png"},
			//items
			{id:"wrench",src:"assets/img/item/wrench.png"},
			{id:"boot", src:"assets/img/item/boot.png"},
			{id:"boot_mud", src:"assets/img/item/boot_mud.png"},
			{id:"keys", src:"assets/img/item/keys.png"},
			//characters
			{id:"mike",src:"assets/img/character/mike.png"},
			{id:"mike_cu_neutral",src:"assets/img/character/mike_cu_neutral.png"},
			
			{id:"sofia",src:"assets/img/character/sofia_full.png"},
			{id:"sofia_cu_neutral",src:"assets/img/character/sofia_cu_neutral.png"},
			
			{id:"jenny",src:"assets/img/character/jenny_full.png"},
			{id:"jenny_cu_neutral",src:"assets/img/character/jenny_cu_neutral.png"},
			
			{id:"sofia_sit",src:"assets/img/character/sofia_sit.png"},
			{id:"sofia_sit_cu_neutral",src:"assets/img/character/sofia_cu_neutral.png"},
			
			{id:"kevin",src:"assets/img/character/kevin_full.png"},
			{id:"kevin_cu_neutral",src:"assets/img/character/kevin_cu_neutral.png"},
			{id:"kevin_phone_cu_neutral",src:"assets/img/character/kevin_phone_cu_neutral.png"},
			
			{id:"other_phone_cu_neutral",src:"assets/img/character/other_phone_cu_neutral.png"},
			{id:"other1_phone_cu_neutral",src:"assets/img/character/other1_phone_cu_neutral.png"},
			{id:"other2_phone_cu_neutral",src:"assets/img/character/other2_phone_cu_neutral.png"}
		]
	);
};

G.ready = function(event){
	//cleaning up the loadbar
	G.logo.style.display = "none";
	G.progressbar.style.display = "none";
	
	//game stuff
	G.scenes.setScene("intro");
	G.inventory.init();
	G.cursor.init();
	//G.inventory.add("keys");
	
	createjs.Ticker.addEventListener("tick", G.tick);
};

G.nothing = function(event){};

G.tick = function(event){
	G.stage.update();
};