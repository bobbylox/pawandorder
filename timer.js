G.Timer = function(delay_seconds, func){
	this.children = [];
	var text = new createjs.Text("","60pt Helvetica", "#000");
	this.addChild(text);
	
	var clockFormat = function(seconds){
		if(seconds<0){
			return "0:00";
		}
		var second_part = seconds%60;
		return ""+Math.floor(seconds/60)+":"+(second_part<10?"0"+second_part:second_part);
	}
	
	var everySecond = function(){
		text.text = clockFormat(delay_seconds);
		if(delay_seconds<0){
			func();
		}
		else{
			createjs.Tween.get(this).wait(1000).call(everySecond);
		}
		
		if(delay_seconds==10){
			text.color = "#F00";
		}
		delay_seconds--;
	}
	createjs.Tween.get(this).wait(1000).call(everySecond);
}
G.Timer.prototype = new createjs.Container();