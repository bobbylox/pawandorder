/*
	here 'data' should take this form:
	{
		"open":{
			"img":"image_name",
			"x":10,   //pixel offsets
			"y":10
		},
		"close":{
			"img":"image_name",
			"x":10,   //pixel offsets
			"y":10
		}
	}
*/

G.Toggle = function(data, onOpen, onClosed){
	this.children = [];
	this.is_open = false;
	var that = this;
	//set images
	var open_img;
	if(data.open){
		open_img = new createjs.Bitmap(G.queue.getResult(data.open.img));
		open_img.x = data.open.x||0;
		open_img.y = data.open.y||0;
		open_img.alpha = 0;
		this.addChild(open_img);
	}
	var close_img;
	if(data.close){
		close_img = new createjs.Bitmap(G.queue.getResult(data.close.img));
		close_img.x = data.close.x||0;
		close_img.y = data.close.y||0;
		close_img.alpha = 1;
		this.addChild(close_img);
	}
	//there's probably a better way to do this - just being super inclusive space-wise
	this.geom = new createjs.Shape();
	var closed_bounds = close_img.getBounds();
	var open_bounds = open_img.getBounds();
	this.geom.graphics.beginFill("#F00").dr(data.close.x||0,data.close.y||0,Math.min(closed_bounds.width,open_bounds.width), Math.min(closed_bounds.height,open_bounds.height));
	//methods
	this.open_up = function(){
		console.log("opening");
		close_img.alpha = 0;
		open_img.alpha = 1;
		that.is_open = true;
		(onOpen||G.nothing)();
	}
	this.close_up = function(){
		console.log("closing");
		open_img.alpha = 0;
		close_img.alpha = 1;
		that.is_open = false;
		(onClosed||G.nothing)();
	}
	this.toggle = function(){
		if(that.is_open){
			that.close_up();
		}
		else{
			that.open_up();
		}
	}
	this.addChild(new G.Hotspot(this.geom, this.toggle, "grab"));
}
G.Toggle.prototype = new createjs.Container();