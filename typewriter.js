G.Typewriter = function(text, font, width, height, time, delay, onComplete){

	this.children = [];
	var that = this;
	//default parameters
	text = text || "";
	font = font || "36pt Courier";
	var margin = 15;
	var screen_bounds = G.stage.getBounds();
	width = width || screen_bounds.width - 2*margin;
	height = height || screen_bounds.height - 2*margin;
	time = time || text.length*50;
	delay = delay || 0;
	onComplete = onComplete || G.nothing;
	
	this.text_object = new createjs.Text("", font, "#000");
	var chars = {c:0};
	
	var add_chars = function(){
		that.text_object.text = text.substr(0,chars.c);
	}                                                                                                                                   
	
	createjs.Tween.get(chars).wait(delay).to({c:text.length}, time).call(function(){createjs.Tween.get(this).wait(500).call(onComplete);}).addEventListener("change",add_chars);
	
	this.addChild(this.text_object);
};
G.Typewriter.prototype = new createjs.Container();